extends "res://addons/gd-plug/plug.gd"


func _plugging():
	# Declare plugins with plug(repo, args)
	# For example, clone from github repo("user/repo_name")
	# plug("imjp94/gd-YAFSM") # By default, gd-plug will only install anything from "addons/" directory
	# Or you can explicitly specify which file/directory to include
	# plug("imjp94/gd-YAFSM", {"include": ["addons/"]}) # By default, gd-plug will only install anything from "addons/" directory

	# Addons/anthonyec.camera_preview
	plug("https://github.com/anthonyec/godot_little_camera_preview")
	# Addons/FileSystemView
	plug("https://github.com/zaevi/godot-filesystem-view")
	# Addons/format-on-save
	plug("https://github.com/ryannhg/gdformat-on-save")
	# Addons/gdlinter
	plug("https://github.com/el-falso/gdlinter")
	# Addons/gd-plug
	plug("https://github.com/imjp94/gd-plug")
	# Addons/gd-plug-ui
	plug("https://github.com/imjp94/gd-plug-ui")
	# Addons/GDScript_Formatter
	plug("https://github.com/Daylily-Zeleen/GDScript-Formatter")
	# Addons/godot_style
	plug("https://github.com/VinhPhmCng/GodotStylePlugin")
	# Addons/godot-git-plugin
	plug("https://github.com/godotengine/godot-git-plugin")
	# Addons/Logger
	plug("https://github.com/albinaask/Log")
	# Addons/MarkdownLabel
	plug("https://github.com/daenvil/MarkdownLabel")
	# Addons/Multirun
	plug("https://github.com/xiezi5160/MultirunForGodot4")
	# Addons/onScreen-Output
	# plug("https://github.com/Maulve/Screen-Output") # can cause issue. must do more tests
	# Addons/open-external-editor
	plug("https://github.com/krayon/godot-addon-open-external-editor")
	# Addons/previous-tab
	plug("https://github.com/MakovWait/godot-previous-tab")
	# Addons/script-ide
	plug("https://github.com/Maran23/script-ide")
