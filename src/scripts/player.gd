class_name Player

extends CharacterBody2D

# associate a name to the keys used for the player movement
const P_KEYS := {"JUMP": "ui_up", "MOVE_LEFT": "ui_left", "MOVE_RIGHT": "ui_right", "DBG_MOVE": "ui_accept"}

@export var movement_data: PlayerMovementData

@export var debug_mode := false
@export var coyote_jump_grace_frame_maxcount := 5

var gravity: float = ProjectSettings.get_setting("physics/2d/default_gravity")
var coyote_jump_grace_frame_count := 0
var can_double_jump := false
var just_wall_jump := false

@onready var animated_sprite_2d := $AnimatedSprite2D


func _physics_process(delta: float) -> void:
	apply_gravity(delta)
	handle_wall_jump()
	handle_jump()
	if debug_mode:
		# allow changing player movement data when pressing the space bar
		if Input.is_action_just_released(P_KEYS.DBG_MOVE):
			var data_name: String = "mouvement_data_default.tres"
			if movement_data.name == "default":
				data_name = "mouvement_data_faster.tres"
			elif movement_data.name == "faster":
				data_name = "mouvement_data_slower.tres"
			else:
				data_name = "mouvement_data_default.tres"
			movement_data = load("res://src/resources/" + data_name)

	var input_axis := Input.get_axis(P_KEYS.MOVE_LEFT, P_KEYS.MOVE_RIGHT)

	apply_acceleration(input_axis, delta)
	apply_air_acceleration(input_axis, delta)
	apply_friction(input_axis, delta)
	apply_air_resistance(input_axis, delta)

	update_animation(input_axis)
	var was_on_floor := is_on_floor()  #check it BEFORE the position is updated by move_and_slide
	move_and_slide()
	var is_falling := velocity.y >= 0
	var just_left_ledge := was_on_floor and not is_on_floor()
	# if the player has just left a ledge and started to fall, we start to count the frames to allow the player
	# to make a jump during a small period
	if just_left_ledge and is_falling:
		coyote_jump_grace_frame_count = 0
	elif input_axis:
		coyote_jump_grace_frame_count += 1
	else:
		coyote_jump_grace_frame_count = coyote_jump_grace_frame_maxcount

	just_wall_jump=false

func apply_gravity(delta: float) -> void:
	if not is_on_floor():
		velocity.y += gravity * movement_data.gravity_scale * delta


func handle_jump() -> void:
	var is_jumpin := velocity.y < movement_data.small_jump_velocity
	if is_on_floor():
		can_double_jump = true

	if is_on_floor() or coyote_jump_grace_frame_count < coyote_jump_grace_frame_maxcount:
		# a long jump: jump pressed when player is on the floor
		if Input.is_action_just_pressed(P_KEYS.JUMP):
			velocity.y = movement_data.jump_velocity

	if not is_on_floor():
		# a shorter jump: jump is released when player is jumping -> divide the velocity (ie the jump height) by 2.
		if is_jumpin and Input.is_action_just_released(P_KEYS.JUMP):
			velocity.y = movement_data.small_jump_velocity

		if Input.is_action_just_pressed(P_KEYS.JUMP) and can_double_jump and not just_wall_jump:
			velocity.y = movement_data.jump_velocity * 0.8
			can_double_jump = false


func handle_wall_jump():
	if not is_on_wall_only():
		return

	var wall_normal := get_wall_normal()
	var jump_left := Input.is_action_just_pressed(P_KEYS.MOVE_LEFT) and wall_normal == Vector2.LEFT
	var jump_right := Input.is_action_just_pressed(P_KEYS.MOVE_RIGHT) and wall_normal == Vector2.RIGHT
	if jump_left or jump_right:
		velocity.x = wall_normal.x * movement_data.speed
		velocity.y = movement_data.jump_velocity
		just_wall_jump = true


func apply_acceleration(input_axis: float, delta: float) -> void:
	if not is_on_floor():
		return
	if input_axis:
		velocity.x = move_toward(velocity.x, input_axis * movement_data.speed, movement_data.acceleration * delta)


func apply_air_acceleration(input_axis: float, delta: float) -> void:
	if is_on_floor():
		return
	if input_axis:
		velocity.x = move_toward(velocity.x, input_axis * movement_data.speed, movement_data.air_acceleration * delta)


func apply_friction(input_axis: float, delta: float) -> void:
	if not is_on_floor():
		return
	if not input_axis:
		velocity.x = move_toward(velocity.x, 0, movement_data.friction * delta)


func apply_air_resistance(input_axis: float, delta: float) -> void:
	if is_on_floor():
		return
	if not input_axis:
		velocity.x = move_toward(velocity.x, 0, movement_data.air_resistance * delta)


func update_animation(input_axis: float) -> void:
	if input_axis:
		animated_sprite_2d.flip_h = input_axis < 0
		if not is_on_floor():
			animated_sprite_2d.play("jump")
		else:
			animated_sprite_2d.play("run")
	else:
		animated_sprite_2d.play("idle")
