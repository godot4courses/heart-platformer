class_name PlayerMovementData
extends Resource

@export var name := "NO_NAME"  # used in debug
# values WILL NOT be multiplied by delta time
@export var speed := 100.0
@export var jump_velocity := -300.0
@export var small_jump_velocity := -150
# values WILL ben multiplied by delta time
@export var friction := 1000
@export var acceleration := 800
@export var gravity_scale := 1.0
@export var air_resistance := 200
@export var air_acceleration := 200
