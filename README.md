# HeartPlatformer

## Description

Prototype de plateformer 2D réalisé dans le cadre du tuto [Godot 4 Tutorial - Heart Platformer](https://www.youtube.com/watch?v=M8-JVjtJlIQ&list=PL9FzW-m48fn0i9GYBoTY-SI3yOBZjH1kJ) par Heartbeast.

## Objectifs

Aucun.

## Mouvements et actions du joueur

Déplacer le joueur: touches ZQSD ou bien les flèches du clavier.
Quitter le jeu: clic sur la croix ou touche escape.

## Interactions

Aucun.

## Copyrights

Codé en GDscript en utilisant le moteur de jeu Godot Engine 4.2.
Développé en utilisant principalement l'éditeur de code intégré et/ou Visual Studio Code.

-----------------
(C) GameaMea Studio (https://www.gameamea.com)
